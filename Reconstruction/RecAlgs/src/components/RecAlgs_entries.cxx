#include "../JobInfo.h"
#include "../EventInfoUnlocker.h"
#include "../AppStopAlg.h"
#include "../JobOptsDumperAlg.h"

DECLARE_COMPONENT( JobInfo )
DECLARE_COMPONENT( EventInfoUnlocker )
DECLARE_COMPONENT( AppStopAlg )
DECLARE_COMPONENT( JobOptsDumperAlg )
